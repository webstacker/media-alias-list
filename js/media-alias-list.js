(function(root, factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define([], factory);
    } else if (typeof module === 'object' && module.exports) {
        // Node. Does not work with strict CommonJS, but
        // only CommonJS-like environments that support module.exports,
        // like Node.
        module.exports = factory();
    } else {
        // Browser globals (root is window)
        root.MediaAliasList = factory();
    }
}(this, function() {
    'use strict';

    function MediaAliasList(alias) {
        this.matches = false;
        this.media = alias;
        this._aliases = alias.split(' ');
        this._listeners = [];
    }
    MediaAliasList.prototype.addListener = function(listener) {
        this._listeners.push(listener);
    };
    MediaAliasList.prototype.removeListener = function(listener) {
        var i = this._listeners.length;

        while(i--) {
            if (listener === this._listeners[i]) {
                this._listeners.splice(i, 1);
            }
        }
    };
    MediaAliasList.prototype._trigger = function() {
        var i;

        for (i = 0; i < this._listeners.length; i++) {
            this._listeners[i](this);
        }
    };

    return MediaAliasList;
}));









