describe('MediaAliasList', function() {

    it('should accept a media alias', function(){
        var mal = new MediaAliasList('small');

        expect(mal._aliases).toEqual(['small'], '_aliases');
        expect(mal.media).toEqual('small', 'media');
    });

    it('should accept multiple space seperated aliases', function(){
        var mal = new MediaAliasList('small medium large');

        expect(mal._aliases).toEqual(['small', 'medium', 'large'], '._aliases');
        expect(mal.media).toEqual('small medium large', '.media');
    });

    it('should add event listener', function(){
        var mal = new MediaAliasList('small medium large'),
            a = function(){},
            b = function(){},
            c = function(){},
            d = function(){},
            e = function(){};

        mal.addListener(a);
        mal.addListener(b);
        mal.addListener(c);
        mal.addListener(d);
        mal.addListener(e);

        expect(mal._listeners).toEqual([a, b, c, d, e]);
    });

    it('should remove event listener', function(){
        var mal = new MediaAliasList('small medium large'),
            a = function(){},
            b = function(){},
            c = function(){},
            d = function(){},
            e = function(){};

        mal.addListener(a);
        mal.addListener(b);
        mal.addListener(c);
        mal.addListener(d);
        mal.addListener(e);

        mal.removeListener(c);

        expect(mal._listeners).toEqual([a, b, d, e]);
    });

    it('should trigger event listeners', function() {
        var mal = new MediaAliasList('small medium large'),
            triggerCount = 0;

        mal.addListener(function(){++triggerCount;});
        mal.addListener(function(){++triggerCount;});
        mal.addListener(function(){++triggerCount;});
        mal.addListener(function(){++triggerCount;});
        mal.addListener(function(){++triggerCount;});

        mal._trigger();

        expect(triggerCount).toEqual(5);
    });
});

